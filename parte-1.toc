\contentsline {chapter}{\'Indice general}{3}
\select@language {spanish}
\contentsline {chapter}{\chapternumberline {1}Software Libre y est\IeC {\'a}ndares abiertos}{4}
\contentsline {section}{\numberline {1.1}Objetivo del curso y plan de trabajo}{5}
\contentsline {section}{\numberline {1.2}Conceptos b\IeC {\'a}sicos}{6}
\contentsline {subsection}{Software y Hardware}{6}
\contentsline {subsection}{Evoluci\IeC {\'o}n del hardware}{8}
\contentsline {subsection}{Evoluci\IeC {\'o}n del software}{10}
\contentsline {subsection}{Comparaci\IeC {\'o}n en la evoluci\IeC {\'o}n entre hardware y software}{12}
\contentsline {section}{\numberline {1.3}Software libre}{13}
\contentsline {subsection}{El software es conocimiento}{13}
\contentsline {subsection}{El software privativo}{15}
\contentsline {subsection}{El software libre}{16}
\contentsline {subsection}{Linux}{18}
\contentsline {subsection}{Software libre, m\IeC {\'a}s que software}{19}
\contentsline {section}{\numberline {1.4}Est\IeC {\'a}ndares abiertos}{20}
\contentsline {chapter}{\chapternumberline {2}La Internet}{22}
\contentsline {section}{\numberline {2.1}Hipertexto y HTML}{23}
\contentsline {section}{\numberline {2.2}El esquema uniforme de nombres}{24}
\contentsline {section}{\numberline {2.3}\IeC {\textquestiondown }Qu\IeC {\'e} es una p\IeC {\'a}gina web?}{26}
\contentsline {section}{\numberline {2.4}\IeC {\textquestiondown }Qu\IeC {\'e} hace un cliente?}{29}
\contentsline {chapter}{\chapternumberline {3}La programaci\IeC {\'o}n web}{31}
\contentsline {section}{\numberline {3.1}HTML}{32}
\contentsline {subsection}{\IeC {\textquestiondown }Cu\IeC {\'a}l el origen de HTML?}{32}
\contentsline {subsection}{El World Wide Web Consortium}{34}
\contentsline {section}{\numberline {3.2}La importancia de los est\IeC {\'a}ndares}{35}
\contentsline {subsection}{\IeC {\textquestiondown }Por qu\IeC {\'e} preocuparse de los est\IeC {\'a}ndares y de la estandarizaci\IeC {\'o}n?}{35}
\contentsline {section}{\numberline {3.3}Elementos y atributos}{36}
\contentsline {subsection}{Elementos y atributos}{37}
\contentsline {subsubsection}{La etiqueta de inicio}{38}
\contentsline {subsubsection}{Elementos vac\IeC {\'\i }os y no vac\IeC {\'\i }os}{39}
\contentsline {subsubsection}{Contenido}{40}
\contentsline {subsubsection}{La etiqueta de cierre}{40}
\contentsline {subsubsection}{Elementos que se cierran por si mismos}{40}
\contentsline {subsubsection}{El problema con las etiquetas o \textit {tags}}{41}
\contentsline {subsubsection}{Sensibilidad a la caja}{41}
\contentsline {subsection}{Elementos \textit {en l\IeC {\'\i }nea} y elementos \textit {en bloque}}{42}
\contentsline {section}{\numberline {3.4}Estructura del documento HTML}{42}
\contentsline {subsection}{La jerarqu\IeC {\'\i }a del contenido}{43}
