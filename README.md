# Curso-Software-Libre-y-desarrollo-web
Curso para estudiantes de colegio acerca del software libre y estándares web

El Curso Software Libre y desarrollo web, es un documento elaborado
dentro de las acciones de la Comunidad de Software Libre de Bolivia,
para facilitar y profundizar el uso de las computadoras Kuaa por parte
de los estudiantes bolivianos de secundaria.

El Curso está compuesto por tres partes.

Cada parte será generada independientemente.

Para generar, se puede usar pdflatex.

pdflatex Curso-Software-Libre-y-desarrollo-web-P-1.tex

pdflatex Curso-Software-Libre-y-desarrollo-web-P-2.tex

pdflatex Curso-Software-Libre-y-desarrollo-web-P-3.tex

