\chapter{La Internet}

\includegraphics[width=1.2\textwidth]{images/chiste2.eps}

\clearpage

Cualquier red de computadoras interconectadas se llama internet
(\textit{\textbf{inter}connected \textbf{net}work}). \comentario{La
  Internet, con mayúscula para diferenciarla de todas las otras
  internets.} Sin embargo, existe una red, más grande que todas las
demás y que interconecta computadoras de todo el mundo. Esa red es
denominada la \textbf{I}nternet y se ha vuelto la red de información
más importante de la historia en el planeta.

\comentario{World = mundo; Wide = ancho; Web = telaraña
  \\ \includegraphics[width=2in]{images/www.eps}}

Si trazáramos líneas líneas entre las computadoras conectadas, se
crearía una especie de telaraña que cubriría el mundo «de punta a
canto». Por eso, en inglés también se la conoce como la \textit{World
  Wide Web}, o www.

Sin embargo, que sólo estén conectadas las computadoras, no significa
nada si no intercambian información. Ese intercambio de información se
realiza mediante archivos digitales (documentos, imágenes, videos,
etc.), llamados \textit{recursos de información}.

\definicion{WWW}{La World Wide Web (Web), es una red de recursos de
  información.}

\comentario{El ingeniero Vannevar Bush escribió «As We May Think» en
  1945, donde describe su concepción del Memex, una máquina que
  pudiera poner en práctica lo que ahora llamamos el hipertexto. Su
  objetivo era ayudar a la humanidad a alcanzar una memoria colectiva
  con una máquina de este tipo y evitar el uso de los descubrimientos
  científicos para la destrucción y la guerra.}

La Web se basa en tres \textit{mecanismos} para hacer que estos
recursos estén listos y a disposición de la mayor audiencia posible:

\begin{itemize}
\item Un \textit{esquema uniforme de nombres} para localizar recursos
  en la Web. (p.ej., URIs).
\item \textit{Protocolos}, para acceder a recursos con nombre en la
  Web (p.ej., HTTP).
\item \textit{Hipertexto}, para navegar fácilmente entre recursos
  (p.ej., HTML).
\end{itemize}

\section{Hipertexto y HTML}

El hipertexto es el mecanismo para pasar de una recurso a otro. Fue
creado por el físico norteamericano Vannevar Bush en 1945. Se
representa como un conjunto de palabras resaltadas que, «activándolas»
(p.e. al hacer «click» con el mouse) \textit{salta} hacia un nuevo
recurso: el apuntado por el hipertexto. Este mecanismo, permite el
intercambio de información a través de Internet mediante una conexión
flexible y fácil de usar.

Para publicar información y distribuirla globalmente, se necesita un
lenguaje entendido universalmente, un lenguaje que incluya el manejo
de hipertexto y que todas las computadoras puedan comprender
potencialmente. El lenguaje de publicación usado por la World Wide Web
es el HTML (acrónimo de HyperText Markup Language, Lenguaje para el
Marcado de Documentos de Hipertexto).

El HTML da a los autores las herramientas para:

\begin{itemize}
\item Publicar documentos en línea con encabezados, textos, tablas,
  listas, fotos, etc.
\item Obtener información en línea a través de vínculos de hipertexto,
  haciendo clic con el botón de un ratón.
\item Diseñar formularios para realizar transacciones con servicios
  remotos, para buscar información, hacer reservas, pedir productos,
  etc.
\item Incluir hojas de cálculo, videoclips, sonidos, y otras
  aplicaciones directamente en sus documentos.
\end{itemize}

\section{El esquema uniforme de nombres}

Si consideramos que en la Internet existen millones de documentos o
recursos de información, hacer referencia a uno de ellos, debe ser
realizado de tal manera que no exista error ni confusión.

Cada recurso tiene un nombre único, una dirección que permite que lo
ubiquemos con precisión. Esa dirección se denomina el
\textit{Universal Resource Identifier}, o «URI», es decir, un
Identificador Universal de Recursos.

El URI, normalmente está compuesto de las siguientes
partes\footnote{Véase información técnica en detalle en
  http://www.ietf.org/rfc/rfc2396.txt y traducido en
  http://www.normes-internet.com/normes.php?rfc=rfc2396\&lang=es}:

\begin{itemize}
\item El protocolo.
Ejemplos:
\begin{itemize}
\item \code{http} (El más común)
\item \code{ftp}
\item \code{gopher}
\end{itemize}
\item El nombre de la máquina.
Ejemplos:
\begin{itemize}
\item \code{www} (El más común)
\item \code{maquina1}
\end{itemize}
\item El dominio.
Ejemplos:
\begin{itemize}
\item \code{softwarelibre.org.bo}
\item \code{yatinaiyambae.org}
\item \code{abisol.org}
\end{itemize}
\item El nombre del recurso, dado en forma de «path» o «ruta de
  acceso».
Ejemplos:
\begin{itemize}
\item \code{wp-content/uploads/2014/08/saberlibre-numero12.pdf}
\item \code{el-camino-del-puma-pilotos-en-aula-con-software-libre}
\end{itemize}
\item Los parámetros.
\item El fragmento.
\end{itemize}

La forma de unirlos, se muestra en la Figura~\ref{fig:uri}. Por
ejemplo, \code{http://softwarelibre.org.bo/saberlibre-numero12.pdf}

\figura{Composición de un URI
  (parcial)}{uri}{uri.eps}{1.2}{Elaboración propia en base al RFC
  2396}

Todas las partes del URI son opcionales. Depende del recurso (donde
está ubicado) y del contexto donde se usa el URI.

\definicion{URI absoluto}{Un URI absoluto tiene protocolo, dominio y
  nombre del recurso }

Cuando usamos un URI dentro de un recurso, podemos hacer referencia a
otros recursos, a partir de la ruta del recurso actual. En esos casos,
se usan los símbolos «\code{.}» (\textit{donde estoy en este momento}) y
«\code{..}» (\textit{un nivel atrás}) para relacionar un recurso a partir
del URI de otro.

\definicion{URI relativo}{Un URI relativo es el que reemplaza al
  protocolo y dominio con referencias al URI de otro recurso.}

\section{¿Qué es una página web?}

Todos hemos visitado y visto miles de páginas web. Pero, a menos que
hayamos hecho una, la naturaleza de lo visto, posiblemente sea un
misterio. Sin embargo, sabemos (vemos) que una página web tiene
«contenidos», es decir, textos, imágenes, sonidos, películas, etc.

\ejercicio{Elabora una lista con las diferentes clases de contenido
  que se pueden encontrar en una página web.}

En realidad, una página web \textit{es un documento HTML}. En este
curso aprenderemos mucho acerca de los documentos HTML, así que aquí
no entraremos en mucho detalle.

Lo que quizás te sorprenda es que el único contenido que realmente
está dentro de la página web es el texto. El resto --como aprenderemos
luego--, simplemente se encuentra \textit{enlazado} a la página
web. Todos los otros contenidos fuera del texto, se encuentran en
archivos separados.

\figura{Modelo Cliente
  Servidor}{cliente-servidor}{cliente-servidor.eps}{0.8}{Elaboración
  propia}

Para poder ver todo ese conjunto de archivos (la página web y los
otros contenidos), se requiere un programa o un mecanismo que
\textit{represente} la información. \comentario{Nos permiten «navegar»
  por la red.} Ese programa, lo llamaremos el \textit{cliente web}. A
esos programas se les conoce también por \textit{browsers} o
navegadores.

Y también se requiere una máquina que entregue los documentos al
cliente. Esa computadora (pueden ser varias), se llama servidor.

\definicion{Cliente web}{Mecanismo que representa un documento web.}

\definicion{Servidor web}{Mecanismo que tiene los archivos HTML y los
  envía al cliente.}

La transmisión de los recursos se realiza \textit{como un flujo} y no
como un todo. Por ejemplo, si el recurso es un archivo, el servidor no
envía todo el archivo de una sola vez, sino como pequeños grupos de
caracteres. El cliente recibe esos grupos y los va ordenando hasta
tener todo el archivo armado como estaba en el servidor. En la
Figura~\ref{fig:flujo} se han representado las tareas del cliente y
del servidor.

\figura{Flujo de dato}{flujo}{flujo.eps}{1}{Elaboración propia}

Por una parte, el servidor toma un archivo de su sistema de archivos y
lo divide en trozos. Esos trozos (técnicamente llamados paquetes)
transitan por la Internet siguiendo diversos caminos.

Una vez que llegan al cliente, éste los arma (como si fuera un
rompecabezas) y luego representa el archivo. En la siguiente sección
hablaremos en detalle lo que hace el cliente.

Por el momento, resulta oportuno definir lo que es un archivo HTML, un
documento HTML, una página web y un sitio web.

\definicion{Archivo HTML, documento HTML, Página web y sitio web}{
  \textit{Archivo web}, es el contenedor dentro del sistema sistema de archivos
  del servidor.\\
  \textit{Documento HTML}, archivo cuyo contenido se ajusta a un
  lenguaje llamado HTML.\\
  \textit{Página web}, es un documento HTML, en el momento en que es
  representado por el cliente.\\
  \textit{Sitio web}, es un conjunto de documentos HTML.}

\section{¿Qué hace un cliente?}

Pensemos un poco más acerca de lo que sucede cuando vemos una página
web en nuestro cliente.

\figura{¿Qué hace un cliente?}{cliente}{cliente.eps}{1}{Elaboración
  propia}

1. Le decimos al cliente que que recupere una página referida por un
URI.

2. A partir de esa solicitud, el cliente envía un requerimiento,
utilizando un protocolo particular llamado http (protocolo de
transferencia de hipertexto). Este requerimiento es enviado al
servidor del recurso indicado por la localización del URI. (Sin buscar
mucha precisión en la definición, un protocolo es una manera en la que
dos dispositivos diferentes transfieren información entre ellos.)

3. El servidor, mediante el TCP/IP (protocolo de control de
transmisión/protocolo internet), devuelve un flujo de bytes (stream),
que el cliente sabe interpretar como HTML (veremos pronto como sabe
eso).

4. Ahora, nuestro cliente está recibiendo un flujo de bytes, el
documento HTML, y decodificándolo. Este proceso de decodificación se
llama \textbf{analizar} (\textit{parser}), debido a que el cliente
descompone el documento en sus elementos gramaticales.

5. Durante el análisis, puede encontrar una parte del documento que
contenga el título, entonces colocará en la ventana del documento un
título.

6. Luego, continúa a lo largo del texto representándolo en la
pantalla. \textbf{Representar} (\textit{render}) es un término
informático, que describe el proceso de dibujar en la pantalla.

7. Durante el análisis, el cliente puede encontrar una imagen. Como se
mencionó anteriormente, la información de la imagen no es, por si
misma, parte del documento HTML. En un documento HTML, las imágenes y
todo otro contenido aparte del texto, es simplemente enlazado (usando
un URI) al documento. Cuando el cliente encuentra un elemento imagen,
envía un requerimiento, vía HTTP, tal como lo hizo para el documento
HTML, pero para el archivo de imagen.

8. Este proceso es continuo y repetido mientras sea necesario, hasta
que la página entera aparece en el cliente.  

